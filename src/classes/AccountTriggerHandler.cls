public with sharing class AccountTriggerHandler extends NF_AbstractTriggerHandler {
  public override void beforeUpdate(){

  }

  public override void afterUpdate(){
      List<Account> lstAccount = new List<Account>();
      List<Contact> lstContactToinsert = new List<Contact>();
      for(Account acct:lstAccount){
          Contact newCon = new Contact();
          newCon.AccountId = acct.id;
          newCon.LastName = acct.Name;
          lstContactToinsert.add(newCon);
      }
      if(lstContactToinsert.size()> 0){
          try{
              insert lstContactToinsert;
          }catch(DMLException e){              
          }
      }
  }

  public override void beforeInsert(){

  }

  public override void afterInsert(){
      list<contact> listOfContactToCreate=new list<contact>();
      for(account accnt:(list<Account>)trigger.new){
          contact contactRec=new contact();
          contactRec.lastname=accnt.name;     
          contactRec.accountid=accnt.id;          
          listOfContactToCreate.add(contactRec);
            
       }
       if(listOfContactToCreate.size()>0){
           insert listOfContactToCreate;
       }

  }

  public override void afterDelete(){

  }

  public override void andFinally(){

  }
}